# This module tries to find libdbusxx library and include files
#
# LIBDBUSXX_INCLUDE_DIR, path where to find dbusxx/dbus.hpp
# LIBDBUSXX_LIBRARY_DIR, path where to find libdbusxx.so
# LIBDBUSXX_LIBRARIES, the library to link against
# LIBDBUSXX_FOUND, If false, do not try to use libdbusxx
#
# This currently works probably only for Linux

FIND_PATH ( LIBDBUSXX_INCLUDE_DIR dbusxx/dbus.hpp
    /usr/local/include
    /usr/include
)

FIND_LIBRARY ( LIBDBUSXX_LIBRARIES dbusxx
    /usr/local/lib
    /usr/lib
)

GET_FILENAME_COMPONENT( LIBDBUSXX_LIBRARY_DIR ${LIBDBUSXX_LIBRARIES} PATH )

SET ( LIBDBUSXX_FOUND "NO" )
IF ( LIBDBUSXX_INCLUDE_DIR )
    IF ( LIBDBUSXX_LIBRARIES )
        SET ( LIBDBUSXX_FOUND "YES" )
    ENDIF ( LIBDBUSXX_LIBRARIES )
ENDIF ( LIBDBUSXX_INCLUDE_DIR )

MARK_AS_ADVANCED(
    LIBDBUSXX_LIBRARY_DIR
    LIBDBUSXX_INCLUDE_DIR
    LIBDBUSXX_LIBRARIES
)
