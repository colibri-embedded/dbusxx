#include <dbusxx/connection.hpp>
#include <dbusxx/exceptions.hpp>
#include <dbus/dbus.h>
#include <iostream>
#include <chrono>

using namespace dbusxx::DBus;

Connection::Connection(BusType bus_type)
    : m_bus_type(bus_type)
    , m_opened(false)
    , m_watcher_running(false)
{
    open();
}

Connection::~Connection()
{
    close();
}

BusType Connection::getBusType() const
{
    return m_bus_type;
}

bool Connection::open()
{
    if(m_opened)
        return true;

    DBusError err;

    dbus_error_init(&err);
    // connect to the bus
    // m_bus_type
    DBusBusType bus_type = DBUS_BUS_SESSION;
    if(m_bus_type == BusType::SYSTEM)
        bus_type = DBUS_BUS_SYSTEM;
    m_conn = dbus_bus_get(bus_type, &err);
    if (dbus_error_is_set(&err)) { 
        dbus_error_free(&err);
        return false;
    }

    dbus_error_free(&err); 
    return true;
}

void Connection::close()
{
    if(!m_opened)
        return;

    stopWatcher();
    dbus_connection_close(m_conn);
    m_opened = false;
}

bool Connection::buildReply(Value& root, DBusMessageIter *rootIter, const std::string& indent)
{
    auto arg_type = dbus_message_iter_get_arg_type(rootIter);

    switch(arg_type) {
        case DBUS_TYPE_ARRAY: {
            DBusMessageIter subIter;
            dbus_message_iter_recurse(rootIter, &subIter);
            
            root.push_back( Value(ValueType::ARRAY) );
            auto& sub = root.back();
            while(buildReply(sub, &subIter, indent + "  "));
            break;
        }
        case DBUS_TYPE_STRUCT: {
            DBusMessageIter subIter;
            dbus_message_iter_recurse(rootIter, &subIter);

            root.push_back( Value(ValueType::STRUCT) );
            auto& sub = root.back();
            while(buildReply(sub, &subIter, indent + "  "));
            break;
        }
        case DBUS_TYPE_DICT_ENTRY: {
            DBusMessageIter subIter;
            dbus_message_iter_recurse(rootIter, &subIter);

            root.push_back(Value(ValueType::DICT_ENTRY));
            auto& dict_entry = root.back();
            // key
            buildReply(dict_entry, &subIter, indent + "  ");
            // value
            buildReply(dict_entry, &subIter, indent + "  ");
            break;
        }
        case DBUS_TYPE_VARIANT: {
            DBusMessageIter subIter;
            dbus_message_iter_recurse(rootIter, &subIter);
            root.push_back(Value(ValueType::VARIANT));
            auto& var = root.back();
            buildReply(var, &subIter, indent + "  ");
            break;
        }

        case DBUS_TYPE_STRING: {
            char* str = nullptr;
            dbus_message_iter_get_basic(rootIter, &str);
            root.push_back(str);
            break;
        }
        case DBUS_TYPE_OBJECT_PATH: {
            char* str = nullptr;
            dbus_message_iter_get_basic(rootIter, &str);
            root.push_back({str, ValueType::OBJECT_PATH});
            break;
        }
        case DBUS_TYPE_SIGNATURE: {
            char* str = nullptr;
            dbus_message_iter_get_basic(rootIter, &str);
            root.push_back({str, ValueType::SIGNITURE});
            break;
        }
        case DBUS_TYPE_BYTE: {
            uint8_t i;
            dbus_message_iter_get_basic(rootIter, &i);
            root.push_back(i);
            break;
        }
        case DBUS_TYPE_INT16: {
            int16_t i;
            dbus_message_iter_get_basic(rootIter, &i);
            root.push_back(i);
            break;
        }
        case DBUS_TYPE_INT32: {
            int32_t i;
            dbus_message_iter_get_basic(rootIter, &i);
            root.push_back(i);
            break;
        }
        case DBUS_TYPE_INT64: {
            int64_t i;
            dbus_message_iter_get_basic(rootIter, &i);
            root.push_back(i);
            break;
        }
        case DBUS_TYPE_UINT16: {
            uint16_t i;
            dbus_message_iter_get_basic(rootIter, &i);
            root.push_back(i);
            break;
        }
        case DBUS_TYPE_UINT32: {
            uint32_t i;
            dbus_message_iter_get_basic(rootIter, &i);
            root.push_back(i);
            break;
        }
        case DBUS_TYPE_UINT64: {
            uint64_t i;
            dbus_message_iter_get_basic(rootIter, &i);
            root.push_back(i);
            break;
        }
        case DBUS_TYPE_UNIX_FD: {
            uint32_t i;
            dbus_message_iter_get_basic(rootIter, &i);
            root.push_back({(uint64_t)i, ValueType::UNIX_FD});
            break;
        }
        case DBUS_TYPE_DOUBLE: {
            double d;
            dbus_message_iter_get_basic(rootIter, &d);
            root.push_back(d);
            break;
        }
        case DBUS_TYPE_BOOLEAN: {
            bool b;
            dbus_message_iter_get_basic(rootIter, &b);
            root.push_back(b);
            break;
        }
    }

    return dbus_message_iter_next(rootIter);
}

void Connection::buildArgs(const Value& root, DBusMessageIter *rootIter)
{

    switch(root.getType()) {
        case ValueType::ARRAY: 
        case ValueType::STRUCT: 
        case ValueType::VARIANT:
        case ValueType::DICT_ENTRY:{
            DBusMessageIter subIter;
            auto sig = root.getDBusSigniture(true);
            if(!dbus_message_iter_open_container(rootIter, root.getDBusType(), sig.c_str(), &subIter)) {
                throw std::runtime_error("Out of memory");
            }

            for(auto& sub: root) {
                buildArgs(sub, &subIter);
            }

            if(!dbus_message_iter_close_container(rootIter, &subIter)) {
                throw std::runtime_error("Out of memory");
            }
            break;
        }

        case ValueType::INT16: {
            auto v = root.as_int16();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::UINT16: {
            auto v = root.as_uint16();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::INT32: {
            auto v = root.as_int32();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::UINT32: {
            auto v = root.as_uint32();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::INT64: {
            auto v = root.as_int64();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::UINT64: {
            auto v = root.as_uint64();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::DOUBLE: {
            auto v = root.as_double();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::BYTE: {
            auto v = root.as_byte();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::BOOLEAN: {
            auto v = root.as_bool()?1:0;
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::UNIX_FD: {
            auto v = root.as_uint32();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &v )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        case ValueType::STRING:
        case ValueType::OBJECT_PATH: 
        case ValueType::SIGNITURE: {
            const char* param = root.as_string().c_str();
            if (!dbus_message_iter_append_basic(rootIter, root.getDBusType(), &param )) { 
                throw std::runtime_error("Out of memory");
            }
            break;
        }
        default:
            break;
    }
}

Value Connection::call(const std::string& target, const std::string& obj_path, const std::string& interface, const std::string& method, const std::vector<Value>& vargs)
{
    DBusMessage* msg;
    DBusMessageIter args;
    DBusPendingCall* pending;

    if(!dbus_validate_path(obj_path.c_str(), nullptr)) {
        throw std::runtime_error("Invalid object path");
    }

    msg = dbus_message_new_method_call(
        target.c_str(), // target for the method call
        obj_path.c_str(), // object to call on
        interface.c_str(), // interface to call on
        method.c_str()); // method name

    if(msg == nullptr) {
        throw std::runtime_error("Out of memory");
    }

    // Append message arguments
    dbus_message_iter_init_append(msg, &args);
    for(const auto& arg: vargs) {
        buildArgs(arg, &args);
    }
    
   // send message and get a handle for a reply
    if (!dbus_connection_send_with_reply (m_conn, msg, &pending, -1)) { // -1 is default timeout
        throw std::runtime_error("Out of memory");
    }

    if(pending == nullptr) {
        dbus_message_unref(msg); 
        throw std::runtime_error("No pending call returned");
    }
    dbus_connection_flush(m_conn);
    // free message
    dbus_message_unref(msg);
    // block until we receive a reply
    dbus_pending_call_block(pending);

    // get the reply message
    msg = dbus_pending_call_steal_reply(pending);

    // free the pending message handle
    dbus_pending_call_unref(pending);

    DBusMessageIter rootIter;
    Value reply(ValueType::STRUCT);
    // read the parameters
    if(dbus_message_iter_init(msg, &rootIter)) {
        buildReply(reply, &rootIter);
    }
    // free reply and close connection
    dbus_message_unref(msg); 

    return reply;
}

unsigned Connection::emit(const std::string& obj_path, const std::string& interface, const std::string& name, const std::vector<Value>& vargs)
{
    dbus_uint32_t serial = 0; // unique number to associate replies with requests
    DBusMessage* msg;
    DBusMessageIter args;

    if(!dbus_validate_path(obj_path.c_str(), nullptr)) {
        throw std::runtime_error("Invalid object path");
    }

    // create a signal and check for errors 
    msg = dbus_message_new_signal(
            obj_path.c_str(), // object name of the signal
            interface.c_str(), // interface name of the signal
            name.c_str()); // name of the signal
    if(msg == nullptr) {
        throw std::runtime_error("Out of memory");
    }

    // Append message arguments
    dbus_message_iter_init_append(msg, &args);
    for(const auto& arg: vargs) {
        buildArgs(arg, &args);
    }

    // send the message and flush the connection
    if (!dbus_connection_send(m_conn, msg, &serial)) { 
        throw std::runtime_error("Out of memory");
    }

    dbus_connection_flush(m_conn);

    // free the message 
    dbus_message_unref(msg);

    return serial;
}

bool Connection::watch(const std::string& path,
                const std::string& interface,
                const std::string& member,
                watcher_cb_t callback)
{
    {
        std::lock_guard<std::mutex> guard(m_watcher_mtx);
        m_watchers.push_back({
            std::regex(path),
            std::regex(interface),
            std::regex(member),
            callback
        });
    }

    startWatcher();
    return true;
}

void Connection::startWatcher()
{
    {
        std::lock_guard<std::mutex> guard(m_watcher_mtx);
        if(m_watcher_running)
            return;
        m_watcher_running = true;
    }
    m_watcher_thread = std::thread(&Connection::watcherLoop, this);
}

void Connection::stopWatcher()
{
    {
        std::lock_guard<std::mutex> guard(m_watcher_mtx);
        if(!m_watcher_running)
            return;
        m_watcher_running = false;
    }
}

void Connection::waitForEnd()
{
    if(m_watcher_thread.joinable())
        m_watcher_thread.join();
}

void Connection::processSignal(const std::string& path, const std::string& interface, const std::string& member, const DBus::Value& data)
{
    std::smatch m;

    for(auto &watcher : m_watchers) {
        if( std::regex_match(path, m, watcher.path_re)
            && std::regex_match(interface, m, watcher.interface_re)
            && std::regex_match(member, m, watcher.member_re)
        ) {
            watcher.callback(path, interface, member, data);
        }
    }
}

void Connection::watcherLoop()
{
    pthread_setname_np(pthread_self(), "dbusxx.watcher" );

    DBusMessage* msg;
    DBusError err;
    // add a rule for which messages we want to see
    dbus_bus_add_match(m_conn, "type='signal'", &err);
    dbus_connection_flush(m_conn);
    if (dbus_error_is_set(&err)) { 
        {
            std::lock_guard<std::mutex>  guard(m_watcher_mtx);
            m_watcher_running = false;
        }

        throw std::runtime_error(err.message);
        return;
    }

    bool running = true;
    while(running) {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for( 10ms );
        // non blocking read of the next available message
        dbus_connection_read_write(m_conn, 0);
        msg = dbus_connection_pop_message(m_conn);

        if(msg != nullptr) {
           if(dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_SIGNAL) {
                const char *member = dbus_message_get_member(msg);
                const char *interface = dbus_message_get_interface(msg);
                const char *path = dbus_message_get_path(msg);

                DBusMessageIter iter;
                Value data(ValueType::STRUCT);

                if(dbus_message_iter_init(msg, &iter)) {
                    // Build reply based on all message arguments
                    while ( buildReply(data, &iter) );                            
                }
                processSignal(path, interface, member, data);
                
           }
        }

        {
            std::lock_guard<std::mutex>  guard(m_watcher_mtx);
            running = m_watcher_running;
        }
    }
}