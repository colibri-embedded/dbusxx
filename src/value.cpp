#include <dbusxx/value.hpp>
#include <dbus/dbus.h>
#include <iostream>

using namespace dbusxx::DBus;

Value::Value(ValueType _type)
    : m_type(_type)
    , m_unsigned(0)
    , m_double(0.0)
    {}

Value::Value(const char* value, ValueType _type)
    : m_type(_type)
    , m_string(value)
    , m_unsigned(0)
    , m_double(0.0)
    {}

Value::Value(const std::string& value, ValueType _type)
    : m_type(_type)
    , m_string(value)
    , m_unsigned(0)
    , m_double(0.0)
    {}

Value::Value(const std::string&& value, ValueType _type)
    : m_type(_type)
    , m_string(std::move(value))
    , m_unsigned(0)
    , m_double(0.0)
    {}

Value::Value(std::uint8_t value, ValueType _type)
    : m_type(_type)
    , m_unsigned(value)
    {}

Value::Value(std::int16_t value, ValueType _type)
    : m_type(_type)
    , m_unsigned(value)
    {}

Value::Value(std::int32_t value, ValueType _type)
    : m_type(_type)
    , m_unsigned(value)
    {}

Value::Value(std::int64_t value, ValueType _type)
    : m_type(_type)
    , m_unsigned(value)
    {}

Value::Value(std::uint16_t value, ValueType _type)
    : m_type(_type)
    , m_unsigned(value)
    {}

Value::Value(std::uint32_t value, ValueType _type)
    : m_type(_type)
    , m_unsigned(value)
    {}

Value::Value(std::uint64_t value, ValueType _type)
    : m_type(_type)
    , m_unsigned(value)
    {}

Value::Value(double value, ValueType _type)
    : m_type(_type)
    , m_double(value)
    {}

Value::Value(bool value, ValueType _type)
    : m_type(_type)
    , m_unsigned(value)
    {}

Value::~Value() {}

void Value::setType(ValueType t)
{
    m_type = t;
}

ValueType Value::getType() const
{
    return m_type;
}

void Value::operator= (const char* value)
{
    m_string = value;
    m_type = ValueType::STRING;
}

void Value::operator= (const std::string& value)
{
    m_string = value;
    m_type = ValueType::STRING;
}

void Value::operator= (const std::string&& value)
{
    m_string = std::move(value);
    m_type = ValueType::STRING;
}

void Value::operator= (std::uint8_t value)
{
    m_unsigned = value;
    m_type = ValueType::BYTE;
}

void Value::operator= (std::int16_t value)
{
    m_unsigned = value;
    m_type = ValueType::INT16;
}

void Value::operator= (std::int32_t value)
{
    m_unsigned = value;
    m_type = ValueType::INT32;
}

void Value::operator= (std::int64_t value)
{
    m_unsigned = value;
    m_type = ValueType::INT64;
}

void Value::operator= (std::uint16_t value)
{
    m_unsigned = value;
    m_type = ValueType::UINT16;
}

void Value::operator= (std::uint32_t value)
{
    m_unsigned = value;
    m_type = ValueType::UINT32;
}

void Value::operator= (std::uint64_t value)
{
    m_unsigned = value;
    m_type = ValueType::UINT64;
}

void Value::operator= (double value)
{
    m_unsigned = value;
    m_type = ValueType::INT16;
}

void Value::operator= (bool value)
{
    m_unsigned = value;
    m_type = ValueType::INT16;
}

Value::reference Value::operator[](size_type pos)
{
    return std::vector<Value::value_type>::at(pos);
}

Value::const_reference Value::operator[](size_type pos) const
{
    return std::vector<Value::value_type>::at(pos);
}

Value::reference Value::operator[](const std::string& key)
{
    if(!this->is_array()) {
        throw std::runtime_error("Value is not an array container");
    }

    for(auto &item: *this) {
        if(item.is_dict_entry()) {
            if(item.at(0).as_string() == key) {
                return item.at(1);
            }
        }
    }

    this->emplace_back(ValueType::DICT_ENTRY);
    auto &new_entry = this->back();
    new_entry.push_back( std::move(Value(key)) );
    new_entry.push_back( std::move(Value()) );

    return new_entry.back();
}

bool Value::contains(const key_type& key) const
{
    if(!this->is_array()) {
        throw std::runtime_error("Value is not an array container");
    }

    for(auto &item: *this) {
        if(item.is_dict_entry()) {
            if(item.at(0).as_string() == key) {
                return true;
            }
        }
    }

    return false;
}

Value::reference Value::at(const size_type& pos)
{
    return std::vector<Value::value_type>::at(pos);
}

Value::const_reference Value::at(const size_type& pos) const
{
    return std::vector<Value::value_type>::at(pos);
}

Value::const_reference Value::at(const std::string& key) const
{
    if(!this->is_array()) {
        throw std::runtime_error("Value is not an array container");
    }

    for(auto &item: *this) {
        if(item.is_dict_entry()) {
            if(item.at(0).as_string() == key) {
                return item.at(1);
            }
        }
    }

    throw std::out_of_range("element '" + key + "' not found");
}

Value::reference Value::at( const std::string& key )
{
    if(!this->is_array()) {
        throw std::runtime_error("Value is not an array container");
    }

    for(auto &item: *this) {
        if(item.is_dict_entry()) {
            if(item.at(0).as_string() == key) {
                return item.at(1);
            }
        }
    }

    throw std::out_of_range("element '" + key + "' not found");
}

Value::reference Value::insert_or_assign_variant(const Value::key_type& key, const Value& value)
{
    if(!this->is_array()) {
        throw std::runtime_error("Value is not an array container");
    }

    auto& variant = (*this)[key];
    if(!variant.is_variant()) {
        variant.setType(ValueType::VARIANT);
    }

    if(variant.size() != 1) {
        variant.clear();
    }

    variant.push_back(value);

    return variant;    
}

/*Value::reference Value::insert_variant(const Value::key_type& key, Value&& value)
{
    if(!this->is_array()) {
        throw std::runtime_error("Value is not an array container");
    }
}

Value::reference Value::insert_variant(Value& value)
{
    if(!this->is_array()) {
        throw std::runtime_error("Value is not an array container");
    }
}

Value::reference Value::insert_variant(Value&& value)
{
    if(!this->is_array()) {
        throw std::runtime_error("Value is not an array container");
    }
}*/


std::uint8_t Value::as_byte() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_byte();
    }
    
    return m_unsigned;
}

std::int16_t Value::as_int16() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_int16();
    }
        
    return m_unsigned;
}

std::int32_t Value::as_int32() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_int32();
    }
        

    return m_unsigned;
}

std::int64_t Value::as_int64() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_int64();
    }
        
    return m_unsigned;
}

std::uint16_t Value::as_uint16() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_uint16();
    }
        
    return m_unsigned;
}

std::uint32_t Value::as_uint32() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_uint32();
    }
        
    return m_unsigned;
}

std::uint64_t Value::as_uint64() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_uint64();
    }
        
    return m_unsigned;
}

bool Value::as_bool() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_bool();
    }
    
    return m_unsigned;
}

double Value::as_double() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_double();
    }
    
    return m_double;
}

std::string Value::as_string() const
{
    if(is_variant()) {
        if(empty()) 
            throw std::out_of_range("Variant has no value assigned");
        return this->at(0).as_string();
    }
    
    return m_string;
}

bool Value::is(ValueType t) const
{
    return m_type == t;
}

bool Value::is_string() const
{
    return m_type == ValueType::STRING;
}

bool Value::is_array() const
{
    return m_type == ValueType::ARRAY;
}

bool Value::is_dict_entry() const
{
    return m_type == ValueType::DICT_ENTRY;
}

bool Value::is_struct() const
{
    return m_type == ValueType::STRUCT;
}

bool Value::is_variant() const
{
    return m_type == ValueType::VARIANT;
}

bool Value::is_number() const
{
    return 
        (  m_type == ValueType::BYTE
        || m_type == ValueType::INT16
        || m_type == ValueType::INT32
        || m_type == ValueType::INT64
        || m_type == ValueType::UINT16
        || m_type == ValueType::UINT32
        || m_type == ValueType::UINT64
        || m_type == ValueType::DOUBLE
        );
}

bool Value::is_double() const
{
    return m_type == ValueType::DOUBLE;
}

bool Value::is_container() const
{
    return
        ( m_type == ValueType::ARRAY
        || m_type == ValueType::DICT_ENTRY
        || m_type == ValueType::STRUCT
        );
}

bool Value::is_invalid() const
{
    return m_type == ValueType::INVALID;
}

int Value::getDBusType() const
{
    switch(m_type){
        // basic
        case ValueType::INT16:
            return DBUS_TYPE_INT16;
        case ValueType::UINT16:
            return DBUS_TYPE_UINT16;
        case ValueType::INT32:
            return DBUS_TYPE_INT32;
        case ValueType::UINT32:
            return DBUS_TYPE_UINT32;
        case ValueType::INT64:
            return DBUS_TYPE_INT64;
        case ValueType::UINT64:
            return DBUS_TYPE_UINT64;
        case ValueType::DOUBLE:
            return DBUS_TYPE_DOUBLE;
        case ValueType::BYTE:
            return DBUS_TYPE_BYTE;
        case ValueType::STRING:
            return DBUS_TYPE_STRING;
        case ValueType::OBJECT_PATH:
            return DBUS_TYPE_OBJECT_PATH;
        case ValueType::BOOLEAN:
            return DBUS_TYPE_BOOLEAN;
        case ValueType::UNIX_FD:
            return DBUS_TYPE_UNIX_FD;
        case ValueType::SIGNITURE:
            return DBUS_TYPE_SIGNATURE;
        // complex
        case ValueType::ARRAY:
            return DBUS_TYPE_ARRAY;
        case ValueType::STRUCT:
            return DBUS_TYPE_STRUCT;
        case ValueType::DICT_ENTRY:
            return DBUS_TYPE_DICT_ENTRY;
        case ValueType::VARIANT:
            return DBUS_TYPE_VARIANT;
        default:
            return DBUS_TYPE_INVALID;
    }
    return DBUS_TYPE_INVALID;
}

std::string Value::getDBusSigniture(bool only_contained) const
{
    std::string signiture;
    switch(m_type){
        // basic
        case ValueType::INT16:
            signiture = DBUS_TYPE_INT16_AS_STRING;
            break;
        case ValueType::UINT16:
            signiture = DBUS_TYPE_UINT16_AS_STRING;
            break;
        case ValueType::INT32:
            signiture = DBUS_TYPE_INT32_AS_STRING;
            break;
        case ValueType::UINT32:
            signiture = DBUS_TYPE_UINT32_AS_STRING;
            break;
        case ValueType::INT64:
            signiture = DBUS_TYPE_INT64_AS_STRING;
            break;
        case ValueType::UINT64:
            signiture = DBUS_TYPE_UINT64_AS_STRING;
            break;
        case ValueType::DOUBLE:
            signiture = DBUS_TYPE_DOUBLE_AS_STRING;
            break;
        case ValueType::BYTE:
            signiture = DBUS_TYPE_BYTE_AS_STRING;
            break;
        case ValueType::STRING:
            signiture = DBUS_TYPE_STRING_AS_STRING;
            break;
        case ValueType::OBJECT_PATH:
            signiture = DBUS_TYPE_OBJECT_PATH_AS_STRING;
            break;
        case ValueType::BOOLEAN:
            signiture = DBUS_TYPE_BOOLEAN_AS_STRING;
            break;
        case ValueType::UNIX_FD:
            signiture = DBUS_TYPE_UNIX_FD_AS_STRING;
            break;
        case ValueType::SIGNITURE:
            signiture = DBUS_TYPE_SIGNATURE_AS_STRING;
            break;
        // complex
        case ValueType::ARRAY:
            signiture = DBUS_TYPE_ARRAY_AS_STRING;
            if(empty()) {
                signiture += DBUS_TYPE_VARIANT_AS_STRING;
            } else {
                if(only_contained) {
                    return front().getDBusSigniture(only_contained);
                }
                signiture += front().getDBusSigniture(only_contained);
            }
            break;
        case ValueType::STRUCT:
            if(!only_contained)
                signiture = "(";
            for(auto &sub: *this) {
                signiture += sub.getDBusSigniture(only_contained);
            }
            if(!only_contained)
                signiture += ")";
            break;
        case ValueType::DICT_ENTRY:
            if(!only_contained)
                signiture += "{";
            if(empty()) {
                signiture += DBUS_TYPE_STRING_AS_STRING;
                signiture += DBUS_TYPE_VARIANT_AS_STRING;
            } else {
                for(auto &sub: *this) {
                    signiture += sub.getDBusSigniture(only_contained);
                }
            }
            if(!only_contained)
                signiture += "}";
            break;
        case ValueType::VARIANT:
            if(empty()) {
                signiture = DBUS_TYPE_VARIANT_AS_STRING;
            } else {
                if(only_contained) {
                    return front().getDBusSigniture(only_contained);
                }
                // signiture += front().getDBusSigniture(only_contained);
                signiture += DBUS_TYPE_VARIANT_AS_STRING;
            }
            break;
        default:
            signiture = DBUS_TYPE_INVALID_AS_STRING;
    }
    return signiture;
}