#include <dbusxx/utils.hpp>
#include <iostream>

namespace dbusxx {
namespace utils {

void print_value(const dbusxx::DBus::Value& v, const std::string& indent)
{
    using namespace dbusxx::DBus;

    switch(v.getType()) {
        case ValueType::ARRAY: {
            std::cout << indent << "@array [" << std::endl;
            for(auto item: v) {
                print_value(item, indent + "    ");
            }
            std::cout << indent << "]" << std::endl;
            break;
        }
        case ValueType::STRUCT: {
            std::cout << indent << "@struct (" << std::endl;
            for(auto item: v) {
                print_value(item, indent + "    ");
            }
            std::cout << indent << ")" << std::endl;
            break;
        }
        case ValueType::DICT_ENTRY: {
            std::cout << indent << "@dict {" << std::endl;
            for(auto item: v) {
                print_value(item, indent + "    ");
            }
            std::cout << indent << "}" << std::endl;
            break;
        }
        case ValueType::VARIANT: {
            std::cout << indent << "@variant\n";
            for(auto item: v) {
                print_value(item, indent + "    ");
            }
            break;
        }
        case ValueType::STRING: {
            std::cout << indent << "@string \"" << v.as_string() << "\"" << std::endl;
            break;
        }
        case ValueType::OBJECT_PATH: {
            std::cout << indent << "@obj-path " << v.as_string() << std::endl;
            break;
        }
        case ValueType::SIGNITURE: {
            std::cout << indent << "@signiture " << v.as_string() << std::endl;
            break;
        }
        case ValueType::BOOLEAN: {
            std::cout << indent << "@boolean " << (v.as_bool()?"true":"false") << std::endl;
            break;
        }
        case ValueType::BYTE: {
            std::cout << indent << "@byte " << (int)v.as_byte() << std::endl;
            break;
        }
        case ValueType::INT16: {
            std::cout << indent << "@int16 " << v.as_int16() << std::endl;
            break;
        }
        case ValueType::INT32: {
            std::cout << indent << "@int32 " << v.as_int32() << std::endl;
            break;
        }
        case ValueType::UNIX_FD: {
            std::cout << indent << "@unix-fd " << v.as_int32() << std::endl;
            break;
        }
        case ValueType::INT64: {
            std::cout << indent << "@int64 " << v.as_int64() << std::endl;
            break;
        }
        case ValueType::UINT16: {
            std::cout << indent << "@uint16 " << v.as_uint16() << std::endl;
            break;
        }
        case ValueType::UINT32: {
            std::cout << indent << "@uint32 " << v.as_uint32() << std::endl;
            break;
        }
        case ValueType::UINT64: {
            std::cout << indent << "@uint64 " << v.as_uint64() << std::endl;
            break;
        }
        default:
            std::cout << indent << "@unknown " << (char)v.getDBusType() << std::endl;
    }
}

std::vector<std::string> splitLine(const std::string& line)
{
    std::vector<std::string> result;
    enum class SplitState {
        SPACE,
        WORD,
        STRING
    };
    SplitState state = SplitState::SPACE;

    unsigned idx=0, last=0;
    for(const auto c : line) {
        if(isspace(c)) {
            switch(state) {
                case SplitState::SPACE:
                    // just continue
                    last = idx;
                    break;
                case SplitState::WORD:
                    // finish word
                    //std::cout << "finish word: " << line.substr(last, idx-last+1) << std::endl;
                    result.push_back( line.substr(last, idx-last) );
                    state = SplitState::SPACE;
                    break;
                case SplitState::STRING:
                    // just continue
                    break;
            }
        } else if(c == '"') {
            // STRING
            switch(state) {
                case SplitState::SPACE:
                    state = SplitState::STRING;
                    // start string
                    last = idx;
                    // std::cout << "start string\n";
                    break;
                case SplitState::WORD:
                    // just continue
                    break;
                case SplitState::STRING:
                    // finish string
                    //std::cout << "finish string: " << line.substr(last+1, idx-last-1) << std::endl;
                    result.push_back( line.substr(last+1, idx-last-1) );
                    last = idx;
                    state = SplitState::SPACE;
                    break;
            }
        } else {
            // WORD
            switch(state) {
                case SplitState::SPACE:
                    // start new word
                    last = idx;
                    state = SplitState::WORD;
                    break;
                case SplitState::WORD:
                    // just continue
                    break;
                case SplitState::STRING:
                    // continue
                    break;
            }
        }
        idx++;
    }

    if(state == SplitState::WORD) {
        if(idx-last > 0) {
            result.push_back( line.substr(last, idx-last+1) );
        }
    }

    return result;
}

} // namespace utils
} // namespace dbusxx