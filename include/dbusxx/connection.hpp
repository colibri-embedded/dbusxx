/**
 * MIT License
 * 
 * Copyright (c) 2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef DBUS_CLIXX_BUS_HPP
#define DBUS_CLIXX_BUS_HPP

#include <string>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <list>
#include <dbusxx/value.hpp>
#include <dbusxx/watcher.hpp>

struct DBusConnection;
struct DBusMessageIter;

namespace dbusxx {
namespace DBus {

    enum class BusType {
        SESSION,
        SYSTEM
    };

    /// Connection interface
    class IConnection {
        public:
            virtual BusType getBusType() const =0;

            virtual Value call(
                const std::string& target,
                const std::string& obj_path,
                const std::string& interface, 
                const std::string& method,
                const std::vector<Value>& args = {}) =0;

            virtual unsigned emit(
                const std::string& obj_path,
                const std::string& interface,
                const std::string& name,
                const std::vector<Value>& args = {}) =0;

            virtual bool watch(
                const std::string& path,
                const std::string& interface,
                const std::string& member,
                watcher_cb_t callback) =0;

            virtual void waitForEnd() =0;

            virtual bool open() =0;
            virtual void close() =0;
    };

    /// Connection imeplentation
    class Connection: public IConnection {
        public:
            /**
             * DBus Bus abstraction constructor
             * 
             * @param _bus Bus session type
             * @param _tool CLI tool to use as backend
             */
            Connection(BusType bus_type = BusType::SESSION);
            ~Connection();

            BusType getBusType() const override;
            
            /**
             * DBus method call
             * 
             * @param dest
             * @param path
             * @param interface
             * @param method
             * @param args
             * 
             * @returns DBus reply or throws exception on error
             */
            Value call(
                const std::string& target,
                const std::string& obj_path,
                const std::string& interface, 
                const std::string& method,
                const std::vector<Value>& args = {}) override;
            /**
             * DBus emit signal
             * 
             */
            unsigned emit(
                const std::string& obj_path,
                const std::string& interface,
                const std::string& name,
                const std::vector<Value>& args = {}) override;
            /**
             * 
             */
            bool watch(
                const std::string& path,
                const std::string& interface,
                const std::string& member,
                watcher_cb_t callback) override;
            /**
             * 
             */
            void waitForEnd() override;

            bool open() override;
            void close() override;

        private:
            BusType m_bus_type;
            DBusConnection* m_conn;
            bool m_opened;
            std::list<Watcher> m_watchers;

            bool m_watcher_running;
            std::mutex m_watcher_mtx;
            std::thread m_watcher_thread;
            
            void startWatcher();
            void stopWatcher();

            void watcherLoop();
            void processSignal(const std::string& path, const std::string& interface, const std::string& member, const DBus::Value& data);
            bool buildReply(Value& root, DBusMessageIter *rootIter, const std::string& indent = "");
            void buildArgs(const Value& root, DBusMessageIter *rootIter);

        protected:
    };

} // namespace DBus
} // namespace dbusclixx

#endif /* DBUS_CONNECTION_HPP */