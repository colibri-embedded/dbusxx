/**
 * MIT License
 * 
 * Copyright (c) 2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef DBUSXX_VALUE_HPP
#define DBUSXX_VALUE_HPP

#include <string>
#include <cstdint>
#include <vector>
#include <memory>
#include <type_traits>

namespace dbusxx {
namespace DBus {

    enum class ValueType {
        INVALID,
        INT16,
        UINT16,
        INT32,
        UINT32,
        INT64,
        UINT64,
        DOUBLE,
        BYTE,
        STRING,
        OBJECT_PATH,
        BOOLEAN,
        ARRAY,
        STRUCT,
        DICT_ENTRY,
        VARIANT,
        UNIX_FD,
        SIGNITURE
    };

    class Value : public std::vector<Value> {
        public:
            typedef std::string key_type;

            explicit Value(ValueType _type=ValueType::INVALID);
            Value(const Value&) =default;
            Value(Value&&) =default;
            ~Value();

            Value(const char* value, ValueType _type=ValueType::STRING);
            Value(const std::string& value, ValueType _type=ValueType::STRING);
            Value(const std::string&& value, ValueType _type=ValueType::STRING);

            Value(std::uint8_t value, ValueType _type=ValueType::BYTE);
            Value(std::int16_t value, ValueType _type=ValueType::INT16);
            Value(std::int32_t value, ValueType _type=ValueType::INT32);
            Value(std::int64_t value, ValueType _type=ValueType::INT64);
            Value(std::uint16_t value, ValueType _type=ValueType::UINT16);
            Value(std::uint32_t value, ValueType _type=ValueType::UINT32);
            Value(std::uint64_t value, ValueType _type=ValueType::UINT16);
            Value(double value, ValueType _type=ValueType::DOUBLE);
            Value(bool value, ValueType _type=ValueType::BOOLEAN);

            void setType(ValueType t);
            ValueType getType() const;
            int getDBusType() const;
            std::string getDBusSigniture(bool only_contained=false) const;

            reference operator= (Value&) =default;
            reference operator= (Value&&) =default;

            void operator= (const char* value);
            void operator= (const std::string& value);
            void operator= (const std::string&& value);
            void operator= (std::uint8_t value);
            void operator= (std::int16_t value);
            void operator= (std::int32_t value);
            void operator= (std::int64_t value);
            void operator= (std::uint16_t value);
            void operator= (std::uint32_t value);
            void operator= (std::uint64_t value);
            void operator= (double value);
            void operator= (bool value);

            bool contains(const key_type& key) const;

            reference at( const size_type& pos );
            const_reference at( const size_type& pos ) const;

            reference at( const key_type& key );
            const_reference at( const key_type& key ) const;

            reference operator[](size_type pos);
            const_reference operator[](size_type pos) const;

            reference operator[](const key_type& key);

            reference insert_or_assign_variant(const key_type& key, const Value& value);
            // reference insert_variant(const key_type& key, Value&& value);
            // reference insert_variant(Value& value);
            // reference insert_variant(Value&& value);

            std::uint8_t as_byte() const;
            std::int16_t as_int16() const;
            std::int32_t as_int32() const;
            std::int64_t as_int64() const;
            std::uint16_t as_uint16() const;
            std::uint32_t as_uint32() const;
            std::uint64_t as_uint64() const;
            bool as_bool() const;
            double as_double() const;
            std::string as_string() const;

            bool is(ValueType t) const;

            bool is_number() const;
            bool is_double() const;
            bool is_string() const;

            bool is_array() const;
            bool is_dict_entry() const;
            bool is_struct() const;
            bool is_variant() const;

            bool is_container() const;
            bool is_invalid() const;

        private:
            ValueType m_type;

            std::string m_string;
            std::uint64_t m_unsigned;
            double m_double;
    };

} // namespace DBus
} // namespace dbusxx

#endif /* DBUSXX_VALUE_HPP */