/**
 * MIT License
 * 
 * Copyright (c) 2019 Colibri-Embedded
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef DBUS_CLIXX_WATCHER_HPP
#define DBUS_CLIXX_WATCHER_HPP

#include <string>
#include <vector>
#include <regex>
#include <functional>
#include <dbusxx/value.hpp>

namespace dbusxx {
namespace DBus {

    typedef std::function<void(const std::string&/*path*/,const std::string&/*interface*/,const std::string&/*member*/, const Value&/*args*/)> watcher_cb_t;

    struct Watcher {
        std::regex path_re;
        std::regex interface_re;
        std::regex member_re;
        watcher_cb_t callback;
    };

} // namespace DBus
} // namespace dbusxx

#endif /* DBUS_CLIXX_WATCHER_HPP */