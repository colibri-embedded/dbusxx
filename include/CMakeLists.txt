# dbusxx library headers
set(LIBRARY_HEADERS
	${PROJECT_SOURCE_DIR}/include/dbusxx/dbus.hpp
	${PROJECT_SOURCE_DIR}/include/dbusxx/connection.hpp
	${PROJECT_SOURCE_DIR}/include/dbusxx/exceptions.hpp
	${PROJECT_SOURCE_DIR}/include/dbusxx/value.hpp
	${PROJECT_SOURCE_DIR}/include/dbusxx/utils.hpp
	${PROJECT_SOURCE_DIR}/include/dbusxx/watcher.hpp
	)

install(FILES ${LIBRARY_HEADERS} DESTINATION "${INSTALL_INC_DIR}")

# Configuration header
configure_file (
	"${PROJECT_SOURCE_DIR}/cmake/template/version.h.in"
	"${PROJECT_BINARY_DIR}/include/dbusxx/version.h"
	)

set(CONFIG_HEADERS
	${PROJECT_BINARY_DIR}/include/dbusxx/version.h
	)

install(FILES ${CONFIG_HEADERS} DESTINATION "${INSTALL_INC_DIR}")