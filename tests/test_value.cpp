#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <dbusxx/value.hpp>
#include <dbusxx/utils.hpp>
#include <dbus/dbus.h>

using namespace dbusxx::DBus;

SCENARIO("Range loop array access", "[value][array-range]") {

    WHEN("Provided an array value with items") {

        Value array(ValueType::ARRAY);
        array.push_back( Value(1) );
        array.push_back( Value(2) );
        array.push_back( Value(3) );

        THEN("It is possible to iterate over the array and access all items") {
            REQUIRE( array.size() == 3 );

            int i=1;
            for(auto &item: array) {
                REQUIRE( item.is_number() );
                REQUIRE( item.as_int32() == i );
                i++;
            }
        }
    }
}

SCENARIO("operator [] item access", "[value][array-range]") {

    WHEN("Provided an array value with items") {

        Value array(ValueType::ARRAY);
        array.push_back( Value(1) );
        array.push_back( Value(2) );
        array.push_back( Value(3) );

        THEN("It is possible access items using [] operator") {

            REQUIRE( array.size() == 3 );

            REQUIRE( array[0].as_bool() );
            REQUIRE( array[0].is_number() );
            REQUIRE( array[0].as_int32() == 1 );

            REQUIRE( array[1].as_bool() );
            REQUIRE( array[1].is_number() );
            REQUIRE( array[1].as_int32() == 2 );

            REQUIRE( array[2].as_bool() );
            REQUIRE( array[2].is_number() );
            REQUIRE( array[2].as_int32() == 3 );
        }
    }
}

SCENARIO("operator [] ref item access", "[value][ref-access]") {

    WHEN("Provided an array value with items") {

        Value array(ValueType::ARRAY);
        array.push_back( Value(1) );
        array.push_back( Value(2) );
        array.push_back( Value(3) );

        array[0] = 5;
        array[1] = 6;
        array[2] = 7;

        THEN("It is possible access items using [] operator by reference") {

            REQUIRE( array.size() == 3 );

            REQUIRE( array[0].as_bool() );
            REQUIRE( array[0].is_number() );
            REQUIRE( array[0].as_int32() == 5 );

            REQUIRE( array[1].as_bool() );
            REQUIRE( array[1].is_number() );
            REQUIRE( array[1].as_int32() == 6 );

            REQUIRE( array[2].as_bool() );
            REQUIRE( array[2].is_number() );
            REQUIRE( array[2].as_int32() == 7 );
        }
    }
}

SCENARIO("at[key] ref item access", "[value][dict-at-access]") {

    WHEN("Provided an dict value with items") {

        Value array(ValueType::ARRAY);

        array["one"] = 1;
        array["one"] = 2;
        array["two"] = 3;

        THEN("It is possible access items using at(key) by reference") {

            REQUIRE( array.size() == 2 );

            REQUIRE( array.at("one").is_number() );
            REQUIRE( array.at("one").as_int32() == 2 );

            REQUIRE( array.at("two").is_number() );
            REQUIRE( array.at("two").as_int32() == 3 );

        }
    }
}

SCENARIO("Signiture of basic types", "[value][basic-signiture]") {

    WHEN("A basic type value is provided") {
        Value i16(-16, ValueType::INT16);
        Value i32(-32, ValueType::INT32);
        Value i64(-64, ValueType::INT64);
        Value u16(16, ValueType::UINT16);
        Value u32(32, ValueType::UINT32);
        Value u64(64, ValueType::UINT64);
        Value s("string");
        Value op("/object/path", ValueType::OBJECT_PATH);
        Value d(1.5);
        Value b(true);

        THEN("Correct signiture is returned") {
            REQUIRE( i16.getDBusSigniture() == DBUS_TYPE_INT16_AS_STRING );
            REQUIRE( i32.getDBusSigniture() == DBUS_TYPE_INT32_AS_STRING );
            REQUIRE( i64.getDBusSigniture() == DBUS_TYPE_INT64_AS_STRING );
            REQUIRE( u16.getDBusSigniture() == DBUS_TYPE_UINT16_AS_STRING );
            REQUIRE( u32.getDBusSigniture() == DBUS_TYPE_UINT32_AS_STRING );
            REQUIRE( u64.getDBusSigniture() == DBUS_TYPE_UINT64_AS_STRING );
            REQUIRE( s.getDBusSigniture() == DBUS_TYPE_STRING_AS_STRING );
            REQUIRE( op.getDBusSigniture() == DBUS_TYPE_OBJECT_PATH_AS_STRING );
            REQUIRE( d.getDBusSigniture() == DBUS_TYPE_DOUBLE_AS_STRING );
            REQUIRE( b.getDBusSigniture() == DBUS_TYPE_BOOLEAN_AS_STRING );
        }
    }
}

SCENARIO("Signiture of array types", "[value][array-signiture]") {

    WHEN("An array type value is provided") {
        Value ai(ValueType::ARRAY);
        ai.push_back({1, ValueType::INT32});

        Value as(ValueType::ARRAY);
        as.push_back("string");

        Value aai(ValueType::ARRAY);
        aai.push_back(ai);

        THEN("Correct signiture is returned") {
            REQUIRE( ai.getDBusSigniture() == "ai" );
            REQUIRE( as.getDBusSigniture() == "as" );
            REQUIRE( aai.getDBusSigniture() == "aai" );
        }
    }
}

SCENARIO("Signiture of struct types", "[value][struct-signiture]") {

    WHEN("A struct type value is provided") {
        Value as(ValueType::ARRAY);
        as.push_back("string");

        Value st0(ValueType::STRUCT);
        st0.push_back({1, ValueType::INT32});
        st0.push_back({1, ValueType::INT32});

        Value st1(ValueType::STRUCT);
        st1.push_back({1, ValueType::INT32});
        st1.push_back({1, ValueType::INT32});
        st1.push_back(as);

        THEN("Correct signiture is returned") {
            REQUIRE( st0.getDBusSigniture() == "(ii)" );
            REQUIRE( st1.getDBusSigniture() == "(iias)" );
        }
    }
}

SCENARIO("Signiture of dict types", "[value][dict-signiture]") {

    WHEN("A dict type value is provided") {
        Value asv(ValueType::ARRAY);
        asv["one"] = Value(ValueType::VARIANT);
        asv["one"].push_back("test");

        Value ass(ValueType::ARRAY);
        ass["one"] = "test";

        THEN("Correct signiture is returned") {
            REQUIRE( asv.getDBusSigniture() == "a{sv}" );
            REQUIRE( ass.getDBusSigniture() == "a{ss}" );
        }
    }
}