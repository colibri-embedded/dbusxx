#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <dbusxx/value.hpp>
#include <dbusxx/utils.hpp>
#include <dbus/dbus.h>

using namespace dbusxx::DBus;

SCENARIO("Assign variant type", "[value][assign-variant]") {

    WHEN("A variant type value is assigned to an array") {
        Value asv1(ValueType::ARRAY);
        asv1["one"] = Value(ValueType::VARIANT);
        asv1["one"].push_back("test");

        Value asv2(ValueType::ARRAY);
        asv2.insert_or_assign_variant("one", "test");

        THEN("Correct signiture is returned") {
            REQUIRE( asv1.getDBusSigniture() == "a{sv}" );
            REQUIRE( asv1.at("one").is_variant() );

            REQUIRE( asv2.getDBusSigniture() == "a{sv}" );
            REQUIRE( asv2.at("one").is_variant() );
        }
    }
}

SCENARIO("Variant signiture", "[value][signiture]") {

    WHEN("A variant type") {
        auto var = Value(ValueType::VARIANT);
        var.push_back("test");

        THEN("Correct signiture is returned") {
            REQUIRE( var.is_variant() );
            REQUIRE( var.getDBusSigniture() == DBUS_TYPE_VARIANT_AS_STRING ); 
        }
    }
}

SCENARIO("Get string value from variant", "[value][read-string]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back("test");

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_string() == "test" ); 
            REQUIRE( var.as_string() == "test" ); 
        }
    }
}

SCENARIO("Get bool value from variant", "[value][read-bool]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back((bool)true);

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_bool() == true ); 
            REQUIRE( var.as_bool() == true ); 
        }
    }
}

SCENARIO("Get double value from variant", "[value][read-double]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back(0.5);

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_double() == 0.5 ); 
            REQUIRE( var.as_double() == 0.5 ); 
        }
    }
}

SCENARIO("Get int16 value from variant", "[value][read-int16]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back(-16);

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_int16() == -16 ); 
            REQUIRE( var.as_int16() == -16 ); 
        }
    }
}

SCENARIO("Get int32 value from variant", "[value][read-int32]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back(-32);

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_int32() == -32 ); 
            REQUIRE( var.as_int32() == -32 ); 
        }
    }
}

SCENARIO("Get int64 value from variant", "[value][read-int64]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back(-64);

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_int64() == -64 ); 
            REQUIRE( var.as_int64() == -64 ); 
        }
    }
}

SCENARIO("Get uint16 value from variant", "[value][read-uint16]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back(16);

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_uint16() == 16 ); 
            REQUIRE( var.as_uint16() == 16 ); 
        }
    }
}

SCENARIO("Get uint32 value from variant", "[value][read-uint32]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back(32);

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_uint32() == 32 ); 
            REQUIRE( var.as_uint32() == 32 ); 
        }
    }
}

SCENARIO("Get uint64 value from variant", "[value][read-uint64]") {

    WHEN("A variant type contains a value") {
        auto var = Value(ValueType::VARIANT);
        var.push_back(64);

        THEN("Correct value can be read") {
            REQUIRE( var.is_variant() );
            REQUIRE( var[0].as_uint64() == 64 ); 
            REQUIRE( var.as_uint64() == 64 ); 
        }
    }
}

SCENARIO("Empty variant is converted", "[value][out-or-range]") {

    WHEN("An empty variant is converted") {
        auto var = Value(ValueType::VARIANT);

        THEN("Correct exception is thrown") {
            REQUIRE_THROWS_AS(var.as_bool(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_byte(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_double(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_string(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_int16(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_int32(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_int64(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_uint16(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_uint32(), std::out_of_range);
            REQUIRE_THROWS_AS(var.as_uint64(), std::out_of_range);
        }
    }
}

