#include <dbusxx/dbus.hpp>
#include <iostream>

using namespace dbusxx;

int main() {
    DBus::Connection conn(DBus::BusType::SYSTEM);
    conn.watch(
        "/org/freedesktop/DBus", "org\\.freedesktop\\.DBus", ".+", 
        [](const std::string& path,const std::string& interface, const std::string& member, const DBus::Value& data){
            std::cout << "SIGNAL path=" << path << ", interface=" << interface << ", member=" << member << std::endl;
            dbusxx::utils::print_value(data);
        }
    );

    conn.waitForEnd();

    return 0;
}