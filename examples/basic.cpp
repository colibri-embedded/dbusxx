#include <dbusxx/dbus.hpp>
#include <iostream>

using namespace dbusxx;

int main() {
    DBus::Connection conn;
    // auto reply = conn.call("org.canonical.Unity", "/org/gnome/Shell", "org.freedesktop.DBus.Properties", "GetAll", {"org.gnome.Shell"});
    auto reply = conn.call("org.kde.plasmashell", "/MainApplication", "org.freedesktop.DBus.Properties", "GetAll", {"org.qtproject.Qt.QGuiApplication"});
    dbusxx::utils::print_value(reply);

    return 0;
}