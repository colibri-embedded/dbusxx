#include <dbusxx/dbus.hpp>
#include <iostream>

using namespace dbusxx;

int main() {
    DBus::Connection conn;
    auto serial = conn.emit("/org/freedesktop/DBus", "org.freedesktop.DBus", "TestSignal", {"string-argument"});

    return 0;
}